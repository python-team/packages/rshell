.TH RSHELL 1 2022-04-14
.
.SH NAME
rshell \- Remote Shell for a MicroPython board
.
.
.SH SYNOPSIS
.SY rshell
.OP \-b baud
.OP \-p port
.OP \-\-rts 1|0
.OP \-\-dtr 1|0
.OP \-u user
.OP \-w password
.OP \-e editor
.OP \-f filename
.OP \-d
.OP \-n
.OP \-l
.OP \-a
.OP \-\-buffer\-size bytes
.OP \-\-wait seconds
.OP \-\-timing
.OP \-\-quiet
.OP cmd
.YS
.
.SY rshell
.B \-h
.YS
.
.SY rshell
.B \-V
.YS
.
.
.SH DESCRIPTION
.B rshell
is a simple shell which runs on the host and uses MicroPython's raw-REPL to
send python snippets to the pyboard in order to get filesystem information, and
to copy files to and from MicroPython's filesystem.
.
.PP
It also has the ability to invoke the regular REPL, so
.B rshell
can be used as a terminal emulator as well.
.
.PP
.B NOTE:
With
.B rshell
you can disable USB Mass Storage and still copy files into and out of your
pyboard.
.
.PP
When using the commands, the
.I /flash
directory, and the
.I /sdcard
directory (if an sdcard is inserted) are considered to be on the pyboard, and
all other directories are considered to be on the host. For an ESP based board
you can only reference its directory by using the board name e.g.
.I /pyboard
etc.
.
.PP
.B NOTE:
.B rshell
requires a fairly recent version of the MicroPython firmware, specifically one
which contains the ubinascii.unhexlify command which was added May 19, 2015
(v1.4.3-28-ga3a14b9 or newer).
.
.PP
If your version of the firmware isn't new enough, then you'll see an error
message something like this:
.
.RS
.PP
.EX
$ rshell
rshell needs MicroPython firmware with ubinascii.unhexlify
.EE
.RE
.
.
.SH OPTIONS
.
.TP
.BR \-h ", " \-\-help
Show a help message and exit
.
.TP
.BR \-a ", " \-\-ascii
On certain platforms the raw REPL mode is unreliable with particular sequences
of binary characters. Specifying
.B \-\-ascii
enables the transfer of binary files to such platforms. It does this by
encoding the data as ASCII hex.
.
.TP
.BR \-b ", " \-\-baud "=\fIBAUD\fR"
Sets the baud rate to use when talking to the pyboard over a serial port. If no
baud is specified, then the baudrate from the
.I RSHELL_BAUD
environment variable is used. If the
.I RSHELL_BAUD
environment variable is not defined then the default baudrate of 115200 is
used.
.
.TP
.BR \-\-buffer\-size "=\fIBUFFER_SIZE\fR"
Sets the buffer size used when transferring files between the host and the
pyboard. If no buffer size is specified, then the value from the
.I RSHELL_BUFFER_SIZE
environment variable is used. If the
.I RSHELL_BUFFER_SIZE
environment variable is not defined, then the default of 512 is used.
.
.TP
.BR \-d ", " \-\-debug
Turns on debugging. This allows you to see the script which is sent over the
raw REPL and the response received.
.
.TP
.BR \-\-dtr "=[\fI0|1|True|False\fR]"
Sets the state of the DTR line when opening the serial port. This may also be
defaulted from the
.I RSHELL_DTR
environment variable.
.
.TP
.BR \-e ", " \-\-editor "=\fIEDITOR\fR"
Specifies the editor to use with the edit command. If no editor is specified,
then the following environment variables will be searched:
.IR RSHELL_EDITOR , VISUAL ", and " EDITOR .
If none of those environment variables is set then vi will be used.
.
.TP
.BR \-f ", " \-\-file "=\fIFILENAME\fR"
Specifies a file of rshell commands to process. This allows you to create a
script which executes any valid rshell commands.
.
.TP
.BR \-n ", " \-\-nocolor
By default, rshell uses ANSI color escape codes when displaying the prompt and
ls output. This option allows colorized output to be disabled.
.
.TP
.BR \-p ", " \-\-port "=\fIPORT\fR"
Specifies the serial port which should be used to talk to the MicroPython
board. You can set the
.I RSHELL_PORT
environment variable to specify the default port to be used, if
.B \-\-port
is not specified on the command line.
.
.TP
.BR \-\-quiet
This option causes the Connecting messages printed when rshell starts to be
suppressed. This is mostly useful for the test scripts.
.
.TP
.BR \-\-rts "=[\fI0|1|True|False\fR]"
Sets the state of the RTS line when opening the serial port. This may also be
defaulted from the
.I RSHELL_RTS
environment variable.
.
.TP
.BR \-\-timing
If the timing option is specified then rshell will print the amount of time
that each command takes to execute.
.
.TP
.BR \-u ", " \-\-user "=\fIUSER\fR"
Specifies the username to use when logging into a WiPy over telnet. If no
username is specified, then the username from the
.I RSHELL_USER
environment variable is used. If the
.I RSHELL_USER
environment variable doesn't exist then the default username 'micro' is used.
.
.TP
.BR \-w ", " \-\-password "=\fIPASSWORD\fR"
Specified the password to use when logging into a WiPy over telnet. If no
password is specified, then the password from the
.I RSHELL_PASSWORD
environment variable is used. If the
.I RSHELL_PASSWORD
environment variable doesn't exist then the default password 'python' is used.
.
.TP
.B \-\-wait
If a port is specified defines how long rshell will wait for the port to exist
and for a connection to be established. The default is 0 seconds specifying an
immediate return.
.
.TP
.B cmd
If a command is specified, then that command will be executed and rshell will
exit.
.
.
.SH FILE SYSTEM
rshell can be connected to multiple pyboards simultaneously. If the board
module exists on the pyboard (i.e. a file named
.I board.py
somewhere in the module search path) and it contains an attribute called name
(e.g. name = "myboard") then the pyboard will use that name. If the board
module can't be imported then the board will be named, pyboard or wipy. Names
will have -1 (or some other number) to make the board name unique.
.
.PP
You can access the internal flash on the first board connected using
.I /flash
and the sd card on the first board connected can be accessed using
.IR /sd .
.
.PP
For all other connected pyboards, you can use
.IR /board-name/flash " or " /board-name/sd
(you can see the board names using the boards command).
.
.PP
The boards command will show all of the connected pyboards, along with all of
the directories which map onto that pyboard.
.
.
.SH COMMANDS
.
.TP
.BR args " [\fIARGUMENTS\fR...]"
Debug function for verifying argument parsing. This function just prints out
each argument that it receives. boards
.
.TP
.BR boards
Lists all of the boards that
.B rshell
is currently connected to, their names, and the connection.
.
.IP
You can give a custom name to a board with either copying over a
.I board.py
file or using the
.B echo
command, e.g.
.
.RS
.IP
.EX
echo 'name="myboard"' > /pyboard/board.py
.EE
.RE
.
.IP
Remember to exit
.B rshell
and re-enter to see the change.
.
.TP
.BR cat " \fIFILENAME\fR..."
Concatenates files and sends to stdout.
.
.TP
.BR cd " \fIDIRECTORY\fR"
Changes the current directory. ~ expansion is supported, and
.B cd -
goes to the previous directory.
.
.TP
.BR connect " \fITYPE\fR \fITYPE_PARAMS\fR"
.
.TQ
.BR connect " serial \fIPORT\fR [\fIBAUD\fR]"
.
.TQ
.BR connect " telnet \fIIP-ADDR-OR-NAME\fR"
Connects a pyboard to
.BR rshell ". " rshell
can be connected to multiple pyboards simultaneously.
.
.TP
.BR cp " \fISOURCE\fR... \fIDEST\fR"
.
.TQ
.BR cp " [" -r | --recursive "] [\fISOURCE\fR|\fISRC_DIR\fR]... \fIDIRECTORY\fR"
.
.TQ
.BR cp " [" -r | --recursive "] \fIPATTERN\fR \fIDIRECTORY\fR"
Copies the
.I SOURCE
file to
.IR DEST ". " DEST
may be a filename or a directory name. If more than one source file is
specified, then the destination should be a directory.
.
.IP
Directories will only be copied if
.B \-\-recursive
is specified.
.
.IP
A single pattern may be specified, in which case the destination should be a
directory. Pattern matching is performed according to a subset of the Unix
rules (see below).
.
.IP
Recursive copying uses the rsync command (see below): where a file exists on
source and destination, it will only be copied if the source is newer than the
destination.
.
.TP
.BR echo " \fITEXT\fR..."
Display a line of text.
.
.TP
.BR edit " \fIFILENAME\fR"
If the file is on a pyboard, it copies the file to host, invokes an editor and
if any changes were made to the file, it copies it back to the pyboard.
.
.IP
The editor which is used defaults to
.BR vi ,
but can be overridden using either the
.B \-\-editor
command line option when rshell is invoked, or by using the
.IR RSHELL_EDITOR ", " VISUAL " or " EDITOR
environment variables (they are tried in the order listed).
.
.TP
.BR filesize " \fIFILE\fR"
Prints the size of the file, in bytes. This function is primarily for testing.
.
.TP
.BR filetype " \fIFILE\fR"
Prints the type of file (dir or file). This function is primarily for testing.
.
.TP
.BR help " [\fICOMMAND\fR]"
List available commands with no arguments, or detailed help when a command is
provided.
.
.TP
.BR ls " [" -a "] [" -l "] [\fIFILE\fR|\fIDIRECTORY\fR|\fIPATTERN\fR]..."
List directory contents. If
.B \-\-long
is specified, a long listing format is used.
.B \-\-all
may be specified to show hidden files. Pattern matching is performed according
to a subset of the Unix rules (see below).
.
.TP
.BR mkdir " \fIDIRECTORY\fR..."
Creates one or more directories.
.
.TP
.BR repl " [\fIBOARD-NAME\fR] [~ \fILINE\fR] [~]"
Enters into the regular REPL with the MicroPython board. Use
.B Control-X
to exit REPL mode and return the shell. It may take a second or two before the
REPL exits.
.
.IP
If you provide a
.I BOARD-NAME
then
.B rshell
will connect to that board, otherwise it will connect to the default board
(first connected board).
.
.IP
If you provide a tilde followed by a space (~ ) then anything after the tilde
will be entered as if you typed it on the command line.
.
.IP
If you want the repl to exit, end the line with the ~ character.
.
.IP
For example, you could use:
.
.RS
.IP
.EX
rshell repl \\~ pyb.bootloader\\(\\)\\~
.EE
.RE
.
.IP
and it will boot the pyboard into DFU.
.
.IP
If you want to execute multiple Python commands these should be separated by
the ~ character (not the ; character):
.
.RS
.IP
.EX
rshell repl \\~ import mymodule \\~ mymodule.run\\(\\)
.EE
.RE
.
.IP
.B NOTE:
Escaping (or quoting) of tilde (and parentheses) is likely to be necessary to
prevent shell-expansion if you are running
.B rshell
from bash. Such escaping is not required if running
.B repl
from within an rshell session.
.
.TP
.BR rm " [" -f | --force "] \fIFILE\fR..."
.
.TQ
.BR rm " [" -f | --force "] \fIPATTERN\fR"
.
.TQ
.BR "rm -r" " [" -f | --force "] \fIPATTERN\fR"
.
.TQ
.BR "rm -r" " [" -f | --force "] [\fIFILE\fR|\fIDIRECTORY\fR]..."
Removes files or directories (including their contents).
.
.IP
A single pattern may be specified. Pattern matching is performed according to a
subset of the Unix rules (see below). Directories can only be removed if the
.B \-\-recursive
argument is provided.
.
.IP
Beware of rm \-r * or worse.
.
.TP
.BR rsync " [" -m | --mirror "] [" -n | --dry-run "] [" -q | --quiet "] \fISRC_DIR\fR \fIDEST_DIR\fR"
Recursively synchronises a source directory to a destination. Directories must
exist.
.
.IP
If
.B \-\-mirror
is specified, files or directories will be removed from the destination if they
are absent in the source.
.B \-\-dry-run
can be specified to report what would be done without making any changes.
.
.IP
Synchronisation is performed by comparing the date and time of source and
destination files. Files are copied if the source is newer than the
destination.
.
.TP
.BR shell " [\fICOMMAND\fR]"
.
.TQ
.BR ! " [\fICOMMAND\fR]"
The shell command can be abbreviated using the exclamation point. This will
invoke a command, and return back to rshell. Example:
.
.RS
.IP
.EX
!make deploy
.EE
.RE
.
.IP
will flash the pyboard.
.
.
.SH PATTERN MATCHING
This is performed according to a subset of the Unix rules. The limitations are
that wildcards are only allowed in the rightmost directory of a path and curly
bracket {} syntax is unsupported:
.
.TP
*.py
Match files in current directory with a .py extension. 
.
.TP
temp/x[0-9]a.*
Match temp/x1a.bmp but not temp/x00a.bmp
.
.TP
t*/*.bmp
Invalid: will produce an error message
.
.TP
{*.doc,*.pdf}
Invalid: will produce an error message
